<?php
session_start();
if(!isset($_SESSION['username'])){
  header('location:index.php');
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dashboard</title>
  <!-- base:css -->
  <link rel="stylesheet" href="./template/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="./template/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="./template/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="./template/images/echalogin.jpeg" />
</head>

<body>
  <div class="container-scroller d-flex">
    <!-- partial:../../partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
       
        <li class="nav-item sidebar-category">
          <p>Components</p>
          <span></span>
        </li>
        
       
        <li class="nav-item">
          <a class="nav-link" href="home.php">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Data Mahasiswa</span>
          </a>
        </li>
        <br>
        <li class="nav-item">
          <a class="nav-link" href="dosen/index.php">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Data Dosen</span>
          </a>
        </li>
        <li class="nav-item sidebar-category">
          <p>Pages</p>
          <span></span>
        </li>
         
          <li class="nav-item">
          <a class="nav-link" href="logout.php">
          <i class="mdi mdi-logout menu-icon"></i>
            <span class="menu-title">logout</span>
          </a>
        </li> 
       

      </ul>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="navbar-brand-wrapper">
            <a class="navbar-brand brand-logo" href="#"><img src="./template/images/logo.svg" alt="logo"/></a>
            <a class="navbar-brand brand-logo-mini" href="#"><img src="./template/images/logo-mini.svg" alt="logo"/></a>
          </div>
          <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1">Welcome back</h4>
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item">
             
            </li>
           
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="../template/images/faces/face4.jpg" alt="image" class="profile-pic">
                  </div>
                 
                </a>
               
              </div>
            </li>
           
          </ul>
          
        </div>
        
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                 <!----button tambah ----->
                 <h1>Selamat datang di tampilan website kami</h1>
                  </div>
                </div>
              </div>
            </div>
           
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="card">
            <div class="card-body">
              <div class="d-sm-flex justify-content-center justify-content-sm-between py-2">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2022</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard </a> templates</span>
              </div>
            </div>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="./template/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="./template/js/jquery.cookie.js" type="text/javascript"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="./template/js/off-canvas.js"></script>
  <script src="./template/js/hoverable-collapse.js"></script>
  <script src="./template/js/template.js"></script>
  <!-- endinject -->
  <!-- End custom js for this page-->
</body>

</html>
