<?php  include "koneksi.php"; 

session_start();
if(!isset($_SESSION['username'])){
  header('location:index.php');
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>tambah data</title>
</head>
<body>
  <div class="container mt-3">
  <div class="card">
  <div class="card-body">
  <h3 class="text-center">masukan data</h3>
<form method="POST" action="proses_create.php" >
<div class="mb-3 row">
    <label for="nama" class="col-sm-2 col-form-label" >nama</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama" name="nama" placeholder="masukan nama">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="nim" class="col-sm-2 col-form-label" >nim</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nim" name="nim" placeholder="masukan nim">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="jurusan" class="col-sm-2 col-form-label" >Jurusan</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="masukan jurusan">
    </div>
  </div>
  
  
  <div class="mb-3 row">
    <label for="fakultas" class="col-sm-2 col-form-label" >fakultas</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="fakultas" name="fakultas" placeholder="masukan fakultas">
    </div>
  </div>
  
 

  <div class="form-group row">
    <label class="col-sm-2 col-form label">&nbsp;</label>
    <div class="col-sm-10">
        <input type="submit"  name="submit" class="btn btn-info" value="simpan">
       
    </div>
  </div>
</form>
    </div>
    </div>
    </div>
</body>
</html>