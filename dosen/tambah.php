<?php
session_start();
if(!isset($_SESSION['username'])){
  header('location:index.php');
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<title>HOME</title>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HOME</title>
  <!-- base:css -->
  <link rel="stylesheet" href="../template/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../template/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../template/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../template/images/echalogin.jpeg" />
</head>
  <!-- Required meta tags -->
 <?php include "../tem/head.php";
 ?>
</head>

<body>
  <div class="container-scroller d-flex">
    <!-- partial:../../partials/_sidebar.html -->
    <?php include "../tem/left_sidebar.php";
 ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      <?php include "../tem/navbar.php";
 ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- bagian atas -->
                  <form method="POST" action="tambah_proses_dosen.php" >
<div class="mb-3 row">
    <label for="nama" class="col-sm-2 col-form-label" >Nama Dosen</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama" name="nama" placeholder="masukan nama">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="pengampu_matkul" class="col-sm-2 col-form-label" >Pengampu Matakuliah</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="pengampu_matkul" name="pengampu_matkul" placeholder="masukan pengampu matkul">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="telp" class="col-sm-2 col-form-label" >No Telepon</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="telp" name="telp" placeholder="masukan telp">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form label">&nbsp;</label>
    <div class="col-sm-10">
        <input type="submit"  name="submit" class="btn btn-info" value="simpan">
        <a href="home.php" class="btn btn-secondary">kembali</a>
    </div>
  </div>
</form>
                  <!-- bagian atas -->
                  
                </div>
              </div>
            </div>
           
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php
        include "../tem/footer.php";
        ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <?php
        include "../tem/script.php";
        ?>
</body>

</html>
