<?php
session_start();
if(!isset($_SESSION['username'])){
  header('location:index.php');
  exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<title>HOME</title>
<head>
  <!-- Required meta tags -->
 <?php include "./tem/head.php";
 ?>
</head>

<body>
  <div class="container-scroller d-flex">
    <!-- partial:../../partials/_sidebar.html -->
    <?php include "./tem/left_sidebar.php";
 ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_navbar.html -->
      <?php include "./tem/navbar.php";
 ?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- bagian atas -->
                  <form method="POST" action="tambah_proses.php" >
<div class="mb-3 row">
    <label for="nama" class="col-sm-2 col-form-label" >nama</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nama" name="nama" placeholder="masukan nama">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="nim" class="col-sm-2 col-form-label" >nim</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="nim" name="nim" placeholder="masukan nim">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="jurusan" class="col-sm-2 col-form-label" >jurusan</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="masukan jurusan">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="fakultas" class="col-sm-2 col-form-label" >fakultas</label>
    <div class="col-sm-10">
    <select class="form-select" name="fakultas" id="fakultas" aria-label="Default select example">
        <option selected>--pilih  fakultas--</option>
        <option value="sistem informasi">teknolgi informasi</option>
        <option value="ekonomi & bisnis">ekonomi & bisnis</option>
        <option value="teknik">teknik</option>
        <option value="hukum">hukum</option>
        <option value="fisip">fisip</option>
        
    </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form label">&nbsp;</label>
    <div class="col-sm-10">
        <input type="submit"  name="submit" class="btn btn-info" value="simpan">
        <a href="home.php" class="btn btn-secondary">kembali</a>
    </div>
  </div>
</form>
                  <!-- bagian atas -->
                  
                </div>
              </div>
            </div>
           
            
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <?php
        include "./tem/footer.php";
        ?>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <?php
        include "./tem/script.php";
        ?>
</body>

</html>
