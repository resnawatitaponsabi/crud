<script src="./template/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="./template/js/jquery.cookie.js" type="text/javascript"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="./template/js/off-canvas.js"></script>
  <script src="./template/js/hoverable-collapse.js"></script>
  <script src="./template/js/template.js"></script>
  <!-- endinject -->
  <!-- End custom js for this page-->